#!/usr/local/bin/vtkpython
import sys
import vtk
import numpy as np

def usage():
    print '+------------------------------------------------------------+'
    print '|            Instant velocity of the electric wave           |'
    print '|                         | BSC |                            |'
    print '| Usage:                                                     |'
    print '|       ./command Case_name NODE_INI NODE_END                |'
    print '|                                                            |'
    print '|This script computes the speed of the electric wave in the  |'
    print '|            direction node_ini->node_end.                   |'
    print '|                                                            |'
    print '|NOTE: if there are "N" nodes, the numeration goes from      |'
    print '|      0 to N-1. The same as Paraview.                       |'
    print '|                                                            |'
    print '|Not workin\'?! Write me a line:                              |'
    print '|Alfonso Santiago - alfonso.santiago@bsc.es                  |'
    print '|Ultra beta version.                                         |'
    print '+------------------------------------------------------------+'
    sys.exit()




if(len(sys.argv)< 3):
    usage()
elif(sys.argv[1]=='-v'):
    usage()


CASENAME = sys.argv[1]
NODE_INI = int(sys.argv[2])
NODE_END = int(sys.argv[3])
#TIMESTEP = int(sys.argv[4])
#PROPERTY = "INTRA"

cdp    = vtk.vtkCompositeDataPipeline()
reader = vtk.vtkGenericEnSightReader() 
reader.SetDefaultExecutivePrototype(cdp)
reader.SetCaseFileName(CASENAME+'.ensi.case')

#reader.ReadAllVariablesOff() # <- "unticks" all the variables
#reader.SetPointArrayStatus(PROPERTY,1) # 1=actived, 0=unactived <- "tick/untick" the selected variable" 
reader.Update()

Tmin       = reader.GetMinimumTimeValue()
Tmax       = reader.GetMaximumTimeValue()
reader.SetTimeValue(Tmax)                         # <- Time at which the value is got.
reader.Update()

if(reader.GetOutput().GetNumberOfBlocks()==1): BLOCKi=0
else: sys.exit()
DATAi = reader.GetOutput().GetBlock(BLOCKi)


Cells      = DATAi.GetNumberOfCells() 
Nodes      = DATAi.GetNumberOfPoints() 

fisoc = np.zeros((Nodes,7))


#print "(1)", reader.GetOutputPort()  # <- Information about the port to get connected.


#set_range  = DATAi.GetScalarRange() # <- Range and center of
#set_center = DATAi.GetCenter()      # <- the values.
#print "(2)", set_range



print '+--------------------+'
print "Time information"
print "Range:  " , Tmin,'->' ,Tmax
print '+--------------------+'

print " "
print '+--------------------+'
print "Geometry information"
print "Number of Cells: " , Cells
print "Number of Nodes: " , Nodes  
print '+--------------------+'


#print "Coordinate point 0: ", DATAi.GetPoint(0) # <- Coordinate point


PTS      = DATAi.GetPointData() #<- Lots of information about the nodes

EMFlag=False
print " "
print '+--------------------+'
print "Readable variables"
for i in range(reader.GetNumberOfVariables()):
  key = reader.GetPointArrayName(i)
  if (key=='DISPL'):
    EMFlag=True
  print key, 
  print reader.GetPointArrayStatus(key)
if(EMFlag):
    print "Electromechanical simulation detected"
else:
    print "Only electrical simulation"
print '+--------------------+'


PTS_PROP = PTS.GetArray("ISOCH")

reader.SetPointArrayStatus("ISOCH",0) 

print " "
print '+--------------------+'
print "Array ISOCH turned off"
print '+--------------------+'

isoc_time=0.0
for n in range (0, Nodes):
     if n==NODE_INI or n==NODE_END:
        isoc_time = PTS_PROP.GetValue(n)

        fisoc[n,0]   = isoc_time
        fisoc[n,1:4] = DATAi.GetPoint(n) 

        if(EMFlag):
            reader.SetTimeValue(isoc_time)
            reader.Update()

            DATAi = reader.GetOutput().GetBlock(BLOCKi)

            PTS_n     = DATAi.GetPointData()
            PTS_displ = PTS_n.GetArray("DISPL")

            fisoc[n,4:7]   = PTS_displ.GetTuple3(n)



print " "
print '+--------------------+'
print "RAW DATA"

print "Node_ini properties:"
print "n_num: ", NODE_INI
print "isoch: ", fisoc[NODE_INI,0]
print "coord: ", fisoc[NODE_INI,1:4]
print "displ: ", fisoc[NODE_INI,4:7]

print " "
print "Node_end properties:"
print "n_num: ", NODE_END
print "isoch: ", fisoc[NODE_END,0]
print "coord: ", fisoc[NODE_END,1:4]
print "displ: ", fisoc[NODE_END,4:7]
print '+--------------------+'

isoch_ini = fisoc[NODE_INI,0]
coord_ini = fisoc[NODE_INI,1:4]
displ_ini = fisoc[NODE_INI,4:7]


isoch_end = fisoc[NODE_END,0]
coord_end = fisoc[NODE_END,1:4]
displ_end = fisoc[NODE_END,4:7]



V_inst = [0,0,0]
V_inst[0] = ((displ_end[0]+coord_end[0])-(displ_ini[0]+coord_ini[0]))/(isoch_end - isoch_ini)
V_inst[1] = ((displ_end[1]+coord_end[1])-(displ_ini[1]+coord_ini[1]))/(isoch_end - isoch_ini)
V_inst[2] = ((displ_end[2]+coord_end[2])-(displ_ini[2]+coord_ini[2]))/(isoch_end - isoch_ini)


print " "
print '+--------------------+'
print "COMPUTED VALUES"
print "V_i(x,y,z) = ((S_end - X_end) - (S_ini - X_ini))/(isoch_end - isoch_ini)"
print V_inst

print " "
print '+--------------------+'
print 'PROGRAM FINISHED'


print "                  /88888888888888888888888888\           "
print "                  |88888888888888888888888888/           "
print "                   |~~____~~~~~~~~~\"\"\"\"\"\"\"\"\"|            "
print "                  / \_________/\"\"\"\"\"\"\"\"\"\"\"\"\"\            "
print "                 /  |              \         \           "
print "                /   |  88    88     \         \          "
print "               /    |  88    88      \         \         "
print "              /    /                  \        |         "
print "             /     |   ________        \       |         "
print "             \     |   \______/        /       |         "
print "  /\"\         \     \____________     /        |         "
print "  | |__________\_        |  |        /        /          "
print "/\"\"\"\"\           \_------\'  \'-------/       --           "
print "\____/,___________\                 -------/             "
print "------*            |                    \                "
print "  ||               |                     \               "
print "  ||               |                 ^    \              "
print "  ||               |                | \    \             "
print "  ||               |                |  \    \            "
print "  ||               |                |   \    \           "
print "  \|              /                /\"\"\"\/    /           "
print "     -------------                |    |    /            "
print "     |\--_                        \____/___/             "
print "     |   |\-_                       |                    "
print "     |   |   \_                     |                    "
print "     |   |     \                    |                    "
print "     |   |      \_                  |                    "
print "     |   |        ----___           |                    "
print "     |   |               \----------|                    "
print "     /   |                     |     ----------\"\"\       "
print "/\"\--\"--_|                     |               |  \      "
print "|_______/                      \______________/    )     "
print "                                              \___/      "

print '+-------------------------------------------------------+'

